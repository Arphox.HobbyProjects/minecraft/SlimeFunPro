﻿using DataAccess;
using SFItemsExporterJsonParsing;
using System;
using System.IO;

namespace RecipeManager
{
    internal class Program
    {
        private static void Main()
        {
            SFItemsExporterModel jsonModel = GetJsonModel();

            using Transformer transformer = new Transformer();
            transformer.PurgeDb();
            transformer.Transform(jsonModel);
            using var db = new RecipeStoreContext();

            Console.ReadKey();
        }

        private static SFItemsExporterModel GetJsonModel()
        {
            ConsoleLogger.WriteLine("Reading json file...");
            const string path = @"C:\Users\ozsva\OneDrive\Desktop\SlimeFun477 recipes.json";
            string json = File.ReadAllText(path);
            ConsoleLogger.WriteLine("Parsing JSON model...");
            var parser = new SFItemsExporterModelParser();
            return parser.Parse(json);
        }
    }
}