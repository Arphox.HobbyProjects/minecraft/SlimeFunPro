﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccess
{
    public sealed class Research
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int Cost { get; set; }

        public List<Item> UnlockedItems { get; set; }
    }
}