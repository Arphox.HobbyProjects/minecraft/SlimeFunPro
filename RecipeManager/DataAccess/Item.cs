﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccess
{
    // TODO: lore?
    public sealed class Item
    {
        public string Id { get; set; }

        public string DisplayName { get; set; }

        [Required]
        public string Material { get; set; }

        public string Category { get; set; }
        public int CategoryTier { get; set; }
        public bool UsableInWorkbench { get; set; }
        public bool Electric { get; set; }
        public bool Radioactive { get; set; }

        public List<Recipe> Recipes { get; set; }
        public Research Research { get; set; }
    }
}