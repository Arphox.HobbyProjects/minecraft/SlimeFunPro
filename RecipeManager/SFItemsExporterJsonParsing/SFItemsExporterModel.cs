﻿namespace SFItemsExporterJsonParsing
{
    public sealed class SFItemsExporterModel
    {
        public SlimeFunItem[] Items { get; set; }
    }
}