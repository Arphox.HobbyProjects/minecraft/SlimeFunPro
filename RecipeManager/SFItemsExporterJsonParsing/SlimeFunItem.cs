﻿namespace SFItemsExporterJsonParsing
{
    public sealed class SlimeFunItem
    {
        public string Id { get; set; }
        public ItemStack Item { get; set; }
        public string Category { get; set; }
        public int CategoryTier { get; set; }
        public string RecipeType { get; set; }
        public ItemStack[] Recipe { get; set; }
        public string Research { get; set; }
        public int ResearchCost { get; set; }
        public bool UsableInWorkbench { get; set; }
        public bool Electric { get; set; }
        public string ElectricType { get; set; }
        public int ElectricCapacity { get; set; }
        public bool Radioactive { get; set; }
        public string RadioactivityLevel { get; set; }
        public string WikiLink { get; set; }
    }
}